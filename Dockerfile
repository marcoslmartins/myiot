FROM maven:3-openjdk-17-slim
EXPOSE 5000
WORKDIR /src
COPY . . 

RUN mvn -Dmaven.test.skip clean package
ARG API_DEPLOY_ENV='production'
ENV API_DEPLOY_ENV=${API_DEPLOY_ENV}
RUN ls -l target/* 

# rename file so that does not depend on version. ex.: rest_api-1.1.0.jar
RUN mv target/mqtt_client-*.jar target/app.jar
ENTRYPOINT ["java" , "-jar", "target/app.jar", "--spring.profiles.active=${API_DEPLOY_ENV}"]

