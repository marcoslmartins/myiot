# IoT Help file
2022-05-31 | César Freire

## Docker 

Start MySQL

`$ docker run -d --name mysql --rm -e MYSQL_ROOT_PASSWORD=123456 -v iot:/var/lib/mysql  -e MYSQL_DATABASE=iot -e MYSQL_USER=iot_user -p 3306:3306 -e MYSQL_PASSWORD=123456 mysql:latest`

Start Redis

`$ docker run -d --name redis --rm -p 6379:6379 redis:alpine`

Compile mqtt_client

`$ docker build -t mqtt_client:<version> .`

Run 
`$ docker run -d --name mqtt_client -p 80:5000 mqtt_client:<version>`

--- 
## Local deploy

activate profile linux  
`$ export spring_profiles_active=<profile>`

activate profile windows  
`$ set spring_profiles_active=<profile>`

local run  
`$ ./mvnw spring-boot:run`

local run with profile  
`$ ./mvnw spring-boot:run -Dspring-boot.run.profiles=development`

run final solution  
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=development`

---

## Local build

clean target  
`$ ./mvnw clean`

target build  
`$ ./mvnw package`

## Local Tests

`$ ./mvnw test`

## Code coverage
`$  ./mvnw verify`

--- 

## CI/CD with gitlab-runner

`$ sudo apk add gitlab-runner`

__production server__  
```
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "9LeBTaChxo8NmeLL_uoh" \
  --executor "shell" \
  --description "docker-lab-node1" \
  --tag-list "production_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
```


__development server__  
```
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "9LeBTaChxo8NmeLL_uoh" \
  --executor "shell" \
  --description "docker-lab-node2" \
  --tag-list "develop_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
```


__test server__  
```
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "9LeBTaChxo8NmeLL_uoh" \
  --executor "docker" \
  --description "docker-lab-node2" \
  --tag-list "develop_server_docker" \
  --docker-image "maven:3-openjdk-17-slim" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
``` 

`$ gitlab-runner run &`