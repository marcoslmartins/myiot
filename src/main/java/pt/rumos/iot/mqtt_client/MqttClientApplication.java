package pt.rumos.iot.mqtt_client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import pt.rumos.iot.mqtt_client.mysql.MysqlSensor;
import pt.rumos.iot.mqtt_client.mysql.MysqlService;
import pt.rumos.iot.mqtt_client.redis.Temperature;
import pt.rumos.iot.mqtt_client.redis.TemperatureService;

@SpringBootApplication
public class MqttClientApplication {

	@Autowired
	TemperatureService service;

	@Autowired
	MysqlService mysqlService;

	public static void main(String[] args) {
        SpringApplication.run(MqttClientApplication.class, args);
	}
	
	@Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }
    
    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {
        return new MessageHandler() {

            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
				long unixTime = System.currentTimeMillis() / 1000L;
                System.out.println(String.format("temperature:%s | timestamp:%d",message.getPayload(), unixTime));
				Temperature t =  new Temperature();
				t.setId(1L);
				t.setTemperature(message.getPayload().toString());
				service.set(t);
				MysqlSensor mysql = new MysqlSensor();
				mysql.setTemperature(message.getPayload().toString());
				mysql.setId(unixTime);
				mysqlService.save(mysql);

            }

        };
    }

    @Bean
    public MessageProducer inbound() {
        MqttPahoMessageDrivenChannelAdapter adapter =
                new MqttPahoMessageDrivenChannelAdapter("tcp://myiot.cfreire.pt:1883", "springClientMartins", "esp32/+/sensors/temperature");
        adapter.setCompletionTimeout(5000);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(1);
        adapter.setOutputChannel(mqttInputChannel());
        return adapter;
    }


}
