package pt.rumos.iot.mqtt_client.mysql;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MysqlRepository extends CrudRepository<MysqlSensor, Long> {
    
}
