package pt.rumos.iot.mqtt_client.redis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

import pt.rumos.iot.mqtt_client.mysql.MysqlSensor;

@Service

public class TemperatureService {
    
    @Autowired
    private TemperatureRepository repo;

    public void set(Temperature t) {
        repo.save(t);
    }

    @ModelAttribute("temperature")
    public Optional<Temperature> findSensor(Long id) {
        return repo.findById(id);
    }

}
