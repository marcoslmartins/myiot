package pt.rumos.iot.mqtt_client.simulator;

public interface Simulator {
    
    public String getBlink();

    public String getCounter();

    public String getRandom();

    public void refresh();
    
}
